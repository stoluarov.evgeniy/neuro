package skala;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import skala.Controllers.AuthorizationDialog;
import skala.Models.UserDetails;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;
import java.net.URL;

public class Neuro extends Application{
    public static UserDetails user_info = new UserDetails();
    public final static URL settingsResource = Neuro.class.getResource("settings_window.fxml");
    public final static URL authorizationResource = Neuro.class.getResource("authorization_window.fxml");
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws IOException {
        try {
            AuthorizationDialog dialog = new AuthorizationDialog();
            user_info = dialog.showAndWait().orElseThrow(() -> new AuthenticationException("Отмена авторизации"));
            user_info.save();
            FXMLLoader fxmlLoader = new FXMLLoader(Neuro.class.getResource("main_window.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 1000, 600);
            stage.setTitle("Hello!");
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.show();
        }
        catch (AuthenticationException ignored){}
    }
    public static void show_error(String error){ show_error(error, null); }
    public static void show_error(String error, String details){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка работы программы");
        alert.setHeaderText(error);
        alert.setContentText(details);
        alert.showAndWait();
    }
}