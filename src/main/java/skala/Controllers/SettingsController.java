package skala.Controllers;

import com.jfoenix.controls.JFXCheckBox;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Models.CallbackAction;
import skala.Models.DeviceInterfaces.*;
import skala.Models.Dictionary.Dictionaries;
import skala.Models.Dictionary.Dictionary;
import skala.Models.Dictionary.DictionaryValue;
import skala.Models.Settings.*;
import skala.Models.TableClasses.TextCell;
import skala.Models.WorkingSetting;
import skala.Neuro;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static skala.Models.Settings.HotKeySetting.KeyEventParser;

/** Контроллер окна настроек */
public class SettingsController{
    private static final Logger logger = LoggerFactory.getLogger(Neuro.class);
    /** Выполнить при подтверждении настроек */
    public CallbackAction reset_settings;
    public JFXCheckBox BDSave_checkbox;
    public JFXCheckBox Autosend_checkbox;
    public JFXCheckBox AutoOpen_checkbox;
    @FXML private Pane photoAreaPanel;
    /** Список словарей */
    @FXML private ListView<Dictionary> dictionaries_list;
    /** Таблица элементов словаря */
    @FXML private TableView<DictionaryValue> dictionary_grid;
    /** Колонка "значение" элемента словаря */
    @FXML private TableColumn<DictionaryValue, Integer> dictionary_grid_value;
    /** Колонка "текст" элемента словаря */
    @FXML private TableColumn<DictionaryValue, String> dictionary_grid_text;
    /** Колонка "приоритет" элемента словаря */
    @FXML private TableColumn<DictionaryValue, Integer> dictionary_grid_num;
    /** Настраиваемое устройство ввода */
    ImageViewer device_viewer;
    /** Список подключенных сканеров */
    List<String> scanner_devices;
    /** Список подключенных камер */
    List<String> camera_devices;
    /** Все возможные режимы работы сканера */
    ScannerModeView scanner_modes = new ScannerModeView();
    /** Корневой узел */
    @FXML private AnchorPane main_tab;
    /** Таблица горячих клавиш */
    @FXML private TableView<HotKeySetting> hot_keys_grid;
    /** Зона для полученного изображения */
    @FXML private ImageView photoArea;
    /** Список устройств ввода */
    @FXML private ListView<String> devicesListView;
    // Настройки сканера
    // Сканируемый прямоугольник
    @FXML public TextField scanner_frame_x;
    @FXML public TextField scanner_frame_y;
    @FXML public TextField scanner_frame_width;
    @FXML public TextField scanner_frame_height;
    /** Список разрешений сканера */
    @FXML public ComboBox<Integer> resolution_combobox;
    /** Список форматов работы сканера */
    @FXML public ComboBox<String> mode_combobox;
    //настройки камеры
    /** Список разрешений камеры */
    @FXML private ListView<DimensionView> dimensionsListView;
    /** Частота работы камеры */
    @FXML private TextField frequencyTextBox;
//окна настроек устройства
    @FXML private TabPane device_settings_tabpane;
    /** Окно настроек сканера */
    @FXML private Tab scanner_settings;
    /** Окно настроек камеры */
    @FXML private Tab camera_setting;
    /** Окно настроек неопознанного устройства */
    @FXML private Tab no_device_settings;
    @FXML
    public void initialize() {
        // инициализация окна словарей
        var dictionaries = Dictionaries.getDictionaries();
        dictionaries_list.getSelectionModel().selectedItemProperty().addListener((observableValue, old_val, new_val) -> {
            if (new_val != null) dictionary_grid.setItems(new_val.getValues());
        });
        dictionaries_list.getItems().setAll(dictionaries.getDictionaryList());

        dictionary_grid_value.setCellFactory(TextCell.forTableColumn(new IntegerStringConverter()));
        dictionary_grid_value.setOnEditCommit(TextCell.getOnEditCommit("setValue"));
        dictionary_grid_text.setCellFactory(TextCell.forTableColumn());
        dictionary_grid_text.setOnEditCommit(TextCell.getOnEditCommit("setText"));
        dictionary_grid_num.setCellFactory(TextCell.forTableColumn(new IntegerStringConverter()));
        dictionary_grid_num.setOnEditCommit(TextCell.getOnEditCommit("setNum"));

        // инициализация окна устройств ввода
        scanner_devices = ScannerViewer.getDevices();
        camera_devices = CameraViewer.getDevices();
        var devises = new ArrayList<String>();
        devises.addAll(scanner_devices);
        devises.addAll(camera_devices);
        devises.add("Сканировать из папки");
        devicesListView.setItems(FXCollections.observableList(devises));

        photoArea.fitWidthProperty().bind(photoAreaPanel.widthProperty());
        photoArea.fitHeightProperty().bind(photoAreaPanel.heightProperty());

        // инициализация окна горячих клавиш
        hot_keys_grid.getSelectionModel().setCellSelectionEnabled(true);
        hot_keys_grid.getItems().setAll(Settings.generate_default_hotkeys_list());

        // загрузка текущих настроек
        Settings.load().ifPresent(settings -> {
            device_viewer = ImageViewer.getDevice(photoArea, settings.getSettings().orElseGet(() -> new FolderSettings(() -> main_tab.getScene().getWindow())));
            hot_keys_grid.getItems().setAll(settings.generate_hotkeys_list());
            BDSave_checkbox.setSelected(settings.getFlag("bd_save"));
            Autosend_checkbox.setSelected(settings.getFlag("auto_parse"));
            AutoOpen_checkbox.setSelected(settings.getFlag("auto_send"));
        });
    }
    @FXML
    protected void testDeviceClick() {
        if (device_viewer!=null) device_viewer.clearDevice();
        try {
            var device = devicesListView.getSelectionModel().getSelectedItem();
            DefaultDeviceSettings setting;
            if (camera_devices.contains(device))    // если выбрана камера
            {
                var dimensions = dimensionsListView.getSelectionModel().getSelectedItem();
                setting = CameraSettings.builder()
                        .camera_name(device)
                        .frequency(Integer.parseInt(frequencyTextBox.getText()))
                        .width(dimensions.getWidth()).height(dimensions.getHeight()).build();

            }else if(scanner_devices.contains(device)){ // если выбран сканер
                setting = ScannerSetting.builder().
                        scanner_name(device).
                        mode(scanner_modes.getValue(mode_combobox.getSelectionModel().getSelectedItem())).
                        resolutions(resolution_combobox.getSelectionModel().getSelectedItem()).
                        x(Integer.parseInt(scanner_frame_x.getText())).y(Integer.parseInt(scanner_frame_y.getText())).
                        width(Integer.parseInt(scanner_frame_width.getText())).height(Integer.parseInt(scanner_frame_height.getText())).build();
            }
            else {
                setting = new FolderSettings(() -> main_tab.getScene().getWindow());
            }
            device_viewer = ImageViewer.getDevice(photoArea, setting);
            device_viewer.start();
            device_viewer.uploadImage();
                // освобождаем ресурсы при закрытии окна
            main_tab.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_HIDING, event -> device_viewer.clearDevice());
        }
        catch (NoSuchElementException ignored){ }
    }
    @FXML
    protected void onDeviceSelect(){
        //TODO хранить объект устройства в списке и переписать выбор в switch с сообщением об ошибке, если устройство не найдено (а не слишком ли много памяти я жру?)
        try {
            var device = devicesListView.getSelectionModel().getSelectedItem();
            CameraViewer.getDevice(device).ifPresentOrElse( // если выбрана камера
                    device_obj -> {
                        device_settings_tabpane.getSelectionModel().select(camera_setting);
                        dimensionsListView.getItems().setAll(Arrays.stream(device_obj.getViewSizes()).map(c -> new DimensionView(c.width, c.height)).toList());
                    },
                    () -> ScannerViewer.getDevice(device).ifPresentOrElse(  // если выбран сканер
                                    device_obj -> {
                                        device_settings_tabpane.getSelectionModel().select(scanner_settings);
                                        mode_combobox.getItems().setAll(scanner_modes.getNames(device_obj.getSupportedModes()));
                                        resolution_combobox.getItems().setAll(device_obj.getSupportedResolutions());
                                    },
                                    () -> device_settings_tabpane.getSelectionModel().select(no_device_settings))
            );
        }
        catch (NoSuchElementException ignored){ }
    }
    /** Сохранение настроек */
    public void onSaveSettings_Click(ActionEvent actionEvent) throws IOException {
        Settings settings = new Settings();
        if(device_viewer != null)
            settings.setSettings(device_viewer.getSettings());
        settings.setHot_keys(hot_keys_grid.getItems());
        settings.setFlag("bd_save", BDSave_checkbox.isSelected());
        settings.setFlag("auto_parse", Autosend_checkbox.isSelected());
        settings.setFlag("auto_send", AutoOpen_checkbox.isSelected());
        settings.save();
        if(reset_settings!=null)
            main_tab.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_HIDING, event -> reset_settings.execute());
        ((Stage)main_tab.getScene().getWindow()).close();
    }
    /** Выбор / смена горячей клавиши */
    public void hotkey_selected(KeyEvent keyEvent) {
        if(Arrays.stream(WorkingSetting.hotkeys_ignore_list).anyMatch(c -> c.equals(keyEvent.getCode()))) return;
        var edited_hokey = hot_keys_grid.getSelectionModel().getSelectedItems().get(0);
        var new_hotkey = KeyEventParser(keyEvent);
        var s = hot_keys_grid.getSelectionModel().getSelectedCells().get(0).getColumn();
        hot_keys_grid.getItems().forEach(c -> c.clear_code(new_hotkey));
        switch (s) {
            case 1 -> edited_hokey.setCode1(new_hotkey);
            case 2 -> edited_hokey.setCode2(new_hotkey);
        }
        hot_keys_grid.refresh();
    }
    /** Добавить элемент в словарь */
    public void addDictionaryValue(ActionEvent actionEvent) {
        dictionary_grid.getItems().add(new DictionaryValue(dictionary_grid.getItems().size(), "Другое"));
    }
    /** Сохранить словарь */
    public void onSaveDictionaries_Click(ActionEvent actionEvent) throws IOException {
        Dictionaries.save_dictionaries(dictionaries_list.getItems().stream().toList());
    }
    /** Удалить значение из словаря */
    public void deleteDictionaryValue(ActionEvent actionEvent) {
        var to_delete = dictionary_grid.getSelectionModel().getSelectedItem();
        if(to_delete!=null) dictionary_grid.getItems().remove(to_delete);
    }
    private void calculatePhotoArea(){
        if(photoArea.getFitWidth() > photoArea.getFitHeight())
            photoArea.setFitWidth(photoAreaPanel.getWidth());
        else
            photoArea.setFitHeight(photoAreaPanel.getHeight());
        photoArea.setLayoutY((photoAreaPanel.getHeight() - photoArea.getFitHeight()) / 2);
    }
}