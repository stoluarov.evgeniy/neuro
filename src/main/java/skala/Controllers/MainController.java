package skala.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToolbar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Models.DeviceInterfaces.FolderSettings;
import skala.Models.DoubleClickEventHandler;
import skala.Models.Settings.HotKeySetting;
import skala.Models.WorkingSetting;
import skala.Neuro;

import java.io.IOException;

import static skala.Neuro.settingsResource;
import static skala.Neuro.show_error;

/** Контроллер - контейнер основного окна */
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(Neuro.class);
    /** Корневой узел */
    @FXML private AnchorPane main_container;
    /** Шапка */
    @FXML private JFXToolbar header;
    /** Контроллер окна сканирования */
    @FXML private ScanningController scanner_viewController;
    /** Окно сканирования */
    @FXML private AnchorPane scanner_view;
    /** Окно результатов */
    @FXML private AnchorPane results_view;
    /** Вкладки */
    @FXML private TabPane tab_pane;
    @FXML private Tab scanner_tab;
    @FXML private Tab results_tab;
    /** Кнопка выбора окна сканирования */
    @FXML private JFXButton open_scannerButton;
    /** Кнопка выбора окна результатов */
    @FXML private JFXButton open_tableButton;
// служебные переменные для отслеживания изменений окна (перетаскивания / масштабирования)
    private double xOffset;
    private double yOffset;
    @FXML
    public void initialize() {
        scanner_view.requestFocus();
        reset_settings();
        header.setOnMouseClicked(new DoubleClickEventHandler((event) -> {}, (event) -> switch_mode()));
    }
    /** Загрузить настройки */
    @FXML
    public void reset_settings(){
        try {
            WorkingSetting.initSettings(new FolderSettings(() -> main_container.getScene().getWindow()));
            scanner_viewController.reload_device();
        }
        catch (Exception ex){
            show_error("Не удалось загрузить настройки", "Пожалуйста, обратитесь к администратору");
            logger.error("MainController - reset_settings()", ex);
        }
    }
    /** Открыть окно результатов */
    @FXML
    public void open_table(ActionEvent actionEvent){
        open_scannerButton.setDisable(false);
        open_tableButton.setDisable(true);
        tab_pane.getSelectionModel().select(results_tab);
        results_view.requestFocus();
    }
    /** Открыть окно сканирования */
    @FXML
    public void open_scanner(ActionEvent actionEvent){
        open_scannerButton.setDisable(true);
        open_tableButton.setDisable(false);
        tab_pane.getSelectionModel().select(scanner_tab);
        scanner_view.requestFocus();
    }
    @FXML
    public void open_settings() {
        if (scanner_viewController.device_viewer != null)
            scanner_viewController.device_viewer.clearDevice();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(settingsResource);
            Scene scene = new Scene(fxmlLoader.load(), 800, 550);
            SettingsController controller = fxmlLoader.getController();
            controller.reset_settings = this::reset_settings;

            Stage stage = new Stage();
            stage.setTitle("Настройки");
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException ex){
            show_error("Не удалось открыть окно настроек", "Пожалуйста, обратитесь к администратору");
            logger.error("MainController - open_settings()", ex);
        }
    }
    /** Закрыть программу */
    @FXML
    public void close(ActionEvent actionEvent){
        if (scanner_viewController.device_viewer != null) scanner_viewController.device_viewer.clearDevice();
        javafx.application.Platform.exit();
    }
    /** Сменить режим окна (во весь экран / в окне) */
    @FXML
    public void switch_mode(){
        var window = (Stage)main_container.getScene().getWindow();
        window.setMaximized(!window.isMaximized());
    }
    /** Скрыть окно */
    @FXML
    public void hide(){ ((Stage)main_container.getScene().getWindow()).setIconified(true); }
// Перемещение окна
    @FXML
    public void stageStartMove(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        xOffset = window.getX() - event.getScreenX();
        yOffset = window.getY() - event.getScreenY();
    }
    @FXML
    public void stageMoving(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        window.setX(event.getScreenX() + xOffset);
        window.setY(event.getScreenY() + yOffset);
    }
// Изменение размеров окна
    @FXML
    public void stageStartSWResize(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        xOffset = window.getX();
        yOffset = window.getY();
    }
    @FXML
    public void stageStartNEResize(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        xOffset = window.getX() + window.getWidth();
        yOffset = window.getY() + window.getHeight();
    }
    @FXML
    public void stageStartNWResize(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        xOffset = window.getX();
        yOffset = window.getY() + window.getHeight();
    }
    @FXML
    public void stageStartSEResize(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        xOffset = window.getX() + window.getWidth();
        yOffset = window.getY();
    }
    @FXML
    public void stageResizing(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        window.setX(Math.min(event.getScreenX(), xOffset));
        window.setY(Math.min(event.getScreenY(), yOffset));
        window.setWidth(Math.abs(event.getScreenX() - xOffset));
        window.setHeight(Math.abs(event.getScreenY() - yOffset));
    }
    @FXML
    public void stageWResizing(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        window.setX(Math.min(event.getScreenX(), xOffset));
        window.setWidth(Math.abs(event.getScreenX() - xOffset));
    }
    @FXML
    public void stageHResizing(MouseEvent event) {
        var window = (Stage)main_container.getScene().getWindow();
        window.setY(Math.min(event.getScreenY(), yOffset));
        window.setHeight(Math.abs(event.getScreenY() - yOffset));
    }
    /** Обработка нажатий горячих клавиш */
    @FXML
    public void scanning_hotkey_click(KeyEvent keyEvent) {
        String keys = HotKeySetting.KeyEventParser(keyEvent);
        String command = WorkingSetting.getHotkeys_map().get(keys);
        if(command == null) return;
        switch (command) {
            case "new_rg" -> scanner_viewController.new_rg();
            case "new_ig" -> scanner_viewController.new_ig();
            case "new_scan" -> scanner_viewController.addNewScanClick();
            case "export_send" -> scanner_viewController.export_person_click();
            case "recognition_send" -> scanner_viewController.recognize_person_click();
            default -> {}
        }
    }
}
