package skala.Controllers;

import com.jfoenix.controls.JFXComboBox;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Import.BDService;
import skala.Import.ImportService;
import skala.Models.DeviceInterfaces.ImageViewer;
import skala.Models.Dictionary.DictionaryConverter;
import skala.Models.Dictionary.DictionaryValue;
import skala.Models.DoubleClickEventHandler;
import skala.Models.LongTermOperationStatus;
import skala.Models.LongTermOperationStatusInterface;
import skala.Models.TableClasses.ComboBoxCell;
import skala.Models.TableClasses.PersonShortView;
import skala.Models.TableClasses.ScanView;
import skala.Models.WorkingSetting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static skala.Neuro.show_error;


public class ScanningController {
    private static final Logger logger = LoggerFactory.getLogger(ScanningController.class);
    public MenuItem addIGContextMenu;
    public MenuItem addRGContextMenu;
    public MenuItem deletePersonContextMenu;
    @FXML private MenuItem addScanContextMenu;
    @FXML private AnchorPane scanning_main_pane;
    /** Окно отображения скана */
    @FXML private ImageView scan_image_view;
    @FXML private StackPane scan_image_panel;
    /** Список граждан */
    @FXML private TableView<PersonShortView> people_tableView;
    /** Колонка типа гражданина */
    @FXML private TableColumn<PersonShortView, DictionaryValue> person_type;
    @FXML private TableColumn<PersonShortView, LongTermOperationStatus> recognition_status_column;
    @FXML private TableColumn<PersonShortView, LongTermOperationStatus> export_status_column;
    /** Выбор типа нового гражданина */
    @FXML private JFXComboBox<DictionaryValue> person_type_combobox;
    /** Выбор типа документа скана */
    @FXML private JFXComboBox<DictionaryValue> doc_type_comboBox;
    /** Таблица сканов */
    @FXML private TableView<ScanView> scans_table;
    @FXML private TableColumn<ScanView, UUID> scan_uid;
    /** Колонка типов сканов */
    @FXML private TableColumn<ScanView, DictionaryValue> scan_type;
    /** Интерфейс для работы с устройством ввода */
    ImageViewer device_viewer;
    //TODO обработать ситуацию, если DictionaryValue примет значение, которого нет в соответствующем Dictionary
    @FXML
    public void initialize() {
        // инициализация окна изображения
        scan_image_view.fitWidthProperty().bind(scan_image_panel.widthProperty());
        scan_image_view.fitHeightProperty().bind(scan_image_panel.heightProperty());

        //инициализация кнопок
        doc_type_comboBox.setConverter(new DictionaryConverter());
        List<DictionaryValue> person_list = new ArrayList<>();
        person_list.add(new DictionaryValue(0, "Новый РГ"));
        person_list.add(new DictionaryValue(1, "Новый ИГ"));
        person_type_combobox.setOnMouseClicked(new DoubleClickEventHandler((event) -> {}, (event) -> addNewPersonClick()));
        person_type_combobox.getSelectionModel().selectedItemProperty().addListener((observableValue, old_value, new_value) ->
        {
            doc_type_comboBox.setItems(new_value.getValue() == 0 ? WorkingSetting.getDocuments_rg() : WorkingSetting.getDocuments_ig());
            doc_type_comboBox.getSelectionModel().select(0);
        });
        person_type_combobox.setConverter(new DictionaryConverter());
        person_type_combobox.getItems().setAll(person_list);
        person_type_combobox.getSelectionModel().select(0);

        // инициализация таблицы граждан
        person_type.setCellValueFactory(ComboBoxCell.getCellValueFactory(List.of(WorkingSetting.getPerson_types()), "type"));
        person_type.setCellFactory(ComboBoxTableCell.forTableColumn(new DictionaryConverter(), WorkingSetting.getPerson_types()));
        export_status_column.setCellValueFactory(new PropertyValueFactory<>("export_status"));
        export_status_column.setCellFactory(column -> new LongTermOperationStatusInterface.StatusCell());
        recognition_status_column.setCellValueFactory(new PropertyValueFactory<>("recognition_status"));
        recognition_status_column.setCellFactory(column -> new LongTermOperationStatusInterface.StatusCell());
        people_tableView.getSelectionModel().selectedItemProperty().addListener((observableValue, old_val, new_val) -> {
            if (new_val != null) {
                scans_table.getSelectionModel().clearSelection();   //TODO убрать баг с неисчезающим dropbox - ом
                var new_doc_list = new_val.getType() == 0 ? WorkingSetting.getDocuments_rg() : WorkingSetting.getDocuments_ig();
                scan_type.setCellValueFactory(ComboBoxCell.getCellValueFactory(new_doc_list, "type"));
                scan_type.setCellFactory(ComboBoxTableCell.forTableColumn(new DictionaryConverter(), new_doc_list));
                scans_table.setItems(new_val.getScans());
            }
        });

        // инициализация таблицы сканов
        scans_table.getSelectionModel().selectedItemProperty().addListener((observableValue, old_val, new_val) -> {
            if (new_val == null) {
                if (device_viewer != null) device_viewer.start();
            } else {
                if (device_viewer != null) device_viewer.stop();
                scan_image_view.setImage(new_val.getImage());
            }
        });
        scans_table.setRowFactory(tv -> {
            TableRow<ScanView> row = new TableRow<>();
            row.setOnMouseClicked(event -> { if(row.isEmpty()) scans_table.getSelectionModel().clearSelection(); });
            return row;
        });
        scan_type.setOnEditCommit(ComboBoxCell.getOnEditCommit("setType"));
    }
    private void initHotKeys(HashMap<String, String> hot_keys){
        if(hot_keys.isEmpty()) return;
        for (var command : hot_keys.keySet())
            switch (hot_keys.get(command)) {
                case "new_scan" -> addScanContextMenu.setAccelerator(KeyCombination.keyCombination(command));
                case "new_rg" -> addRGContextMenu.setAccelerator(KeyCombination.keyCombination(command));
                case "new_ig" -> addIGContextMenu.setAccelerator(KeyCombination.keyCombination(command));
                case "export_send" -> deletePersonContextMenu.setAccelerator(KeyCombination.keyCombination(command));
                default -> { }
            }
    }
    /** Загрузка настроек */
    @FXML
    public void reload_device(){
        if (device_viewer != null) device_viewer.clearDevice();
        try {
            device_viewer = ImageViewer.getDevice(scan_image_view, WorkingSetting.getDevice_settings());
            if(device_viewer != null) device_viewer.start();
            else show_error("Не удалось идентифицировать устройство ввода", "Обратитесь к администратору");
        }
        catch (Exception ex){
            show_error("Не удалось подключить сканер", "Проверьте подключение к устройству, выбранному в настройках программы");
            logger.error("Не удалось инициализировать устройство", ex);
        }
        scan_type.setCellValueFactory(ComboBoxCell.getCellValueFactory(WorkingSetting.getDocuments_all(), "type"));
        scan_type.setCellFactory(ComboBoxTableCell.forTableColumn(new DictionaryConverter(), WorkingSetting.getDocuments_all()));
        doc_type_comboBox.getSelectionModel().select(0);
        if(WorkingSetting.getFlags().bd_save) reload_bd_connection();
        initHotKeys(WorkingSetting.getHotkeys_map());
    }
    public void reload_bd_connection(){
        people_tableView.getItems().setAll(BDService.getPeople().stream().map(PersonShortView::new).toList());
    }
    /** Добавление нового гражданина */
    @FXML
    public void addNewPersonClick(){
        scans_table.getSelectionModel().clearSelection();
        var new_person = new PersonShortView(person_type_combobox.getSelectionModel().getSelectedItem().getValue());
        if(!WorkingSetting.getFlags().bd_save || BDService.addPerson(new_person.toPerson())) {
            people_tableView.getItems().add(new_person);
            people_tableView.getSelectionModel().select(new_person);
        }
    }
    /** Добавление нового скана */
    @FXML
    public void addNewScanClick(){
        if(device_viewer == null) {
            show_error("Не найдено устройство ввода");
            return;
        }
        scans_table.getSelectionModel().clearSelection();   // выход из режима просмотра существующих сканов
        var selected_list = people_tableView.getSelectionModel().getSelectedItems();
        if(selected_list.isEmpty()) addNewPersonClick();   // добавляем нового гражданина, если нет выбранного
        var person = selected_list.get(0);
        try {
            ((List<javafx.scene.image.Image>) device_viewer.getImage()).forEach(image -> {
                var scan = new ScanView(doc_type_comboBox.getSelectionModel().getSelectedItem().getValue(), image);
                person.addScan(scan);
            });
            people_tableView.refresh();
        }
        catch (Exception ex){
            show_error("Ошибка работы с устройтсвом. Проверьте подключение");
        }
    }
    public void export_person_click(){
        if(!people_tableView.getSelectionModel().getSelectedItems().isEmpty())
            ImportService.sendPersonAsync(people_tableView.getSelectionModel().getSelectedItems().get(0));
    }
    public void delete_person_click(){
        if(!WorkingSetting.getFlags().bd_save || BDService.deletePerson(people_tableView.getSelectionModel().getSelectedItem().getId())) {
            people_tableView.getSelectionModel().getSelectedItem().removeAllScans();
            people_tableView.getItems().remove(people_tableView.getSelectionModel().getSelectedItem());
        }
    }
    public void recognize_person_click(){
        if(!people_tableView.getSelectionModel().getSelectedItems().isEmpty())
            ImportService.recognizePersonAsync(people_tableView.getSelectionModel().getSelectedItems().get(0));
    }
    /** Обработка стандартных горячих клавиш */
    public void onScanTable_HotkeyClick(KeyEvent keyEvent) {
        switch (keyEvent.getCode()){
            case DELETE -> {
                var scan = scans_table.getSelectionModel().getSelectedItem();
                people_tableView.getSelectionModel().getSelectedItem().removeScan(scan);
            }
            case ESCAPE -> scans_table.getSelectionModel().clearSelection();
        }
    }
    /** Обработка стандартных горячих клавиш */
    public void onPeopleList_HotkeyClick(KeyEvent keyEvent) {
        switch (keyEvent.getCode()){
            case DELETE -> delete_person_click();
            case ESCAPE -> people_tableView.getSelectionModel().clearSelection();
        }
    }
    @FXML
    void new_rg(){ person_type_combobox.getSelectionModel().select(0); }
    @FXML
    void new_ig(){ person_type_combobox.getSelectionModel().select(1); }
}