package skala.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Models.Dictionary.DictionaryConverter;
import skala.Models.Dictionary.DictionaryValue;
import skala.Models.TableClasses.*;
import skala.Models.WorkingSetting;
import skala.Neuro;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class TableViewController {
    private static final Logger logger = LoggerFactory.getLogger(Neuro.class);
    @FXML public TableView<PersonFullView> person_full_table;
    @FXML public TableColumn<PersonFullView, DictionaryValue> person_type_column;
    @FXML public TableColumn<PersonFullView, String> surname_column;
    @FXML public TableColumn<PersonFullView, String> name_column;
    @FXML public TableColumn<PersonFullView, String> middlename_column;
    @FXML public TableColumn<PersonFullView, LocalDate> date_from_column;
    @FXML public TableColumn<PersonFullView, LocalDate> date_to_column;
    @FXML
    public void initialize(){
        person_full_table.getSelectionModel().setCellSelectionEnabled(true);
        person_type_column.setCellValueFactory(ComboBoxCell.getCellValueFactory(Arrays.asList(WorkingSetting.getPerson_types()), "type"));
        person_type_column.setCellFactory(ComboBoxTableCell.forTableColumn(new DictionaryConverter(), WorkingSetting.getPerson_types()));
        person_type_column.setOnEditCommit(ComboBoxCell.getOnEditCommit("setType"));
        surname_column.setCellFactory(TextCell.forTableColumn());
        name_column.setCellFactory(TextCell.forTableColumn());
        middlename_column.setCellFactory(TextCell.forTableColumn());
        date_from_column.setCellFactory(DateCell.forTableColumn());
        date_to_column.setCellFactory(DateCell.forTableColumn());

        var list = new ArrayList<PersonFullView>();
        list.add(new PersonFullView(PeopleType.RUSSIAN, "Фамилия", "Имя", "Отчество"));
        person_full_table.getItems().setAll(list);
    }
}
