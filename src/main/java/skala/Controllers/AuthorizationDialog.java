package skala.Controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Window;
import skala.Models.UserDetails;
import skala.Neuro;

import java.io.IOException;
import java.util.Objects;

import static skala.Neuro.authorizationResource;

public class AuthorizationDialog extends Dialog<UserDetails> {
    public TextField loginTextBox;
    public TextField passwordTextBox;
    public ButtonType OK_button;
    private UserDetails result;

    public AuthorizationDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(authorizationResource);
            loader.setController(this);

            DialogPane dialogPane = loader.load();
            dialogPane.lookupButton(OK_button).addEventFilter(ActionEvent.ACTION, this::onSubmit);
            initModality(Modality.APPLICATION_MODAL);

            setResizable(true);
            setTitle("Авторизация");
            setDialogPane(dialogPane);
            setResultConverter(buttonType -> {
                if(!Objects.equals(ButtonBar.ButtonData.OK_DONE, buttonType.getButtonData())) {
                    return result;
                }
                return null;
            });
            setOnShowing(dialogEvent -> Platform.runLater(() -> loginTextBox.requestFocus()));
            UserDetails.load().ifPresent(saved -> {
                loginTextBox.setText(saved.getLogin());
                passwordTextBox.setText(saved.getPassword());
            });
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void onSubmit(ActionEvent event) {
        try{
            result = new UserDetails(loginTextBox.getText(), passwordTextBox.getText());
        }
        catch (IllegalArgumentException ex) {
            event.consume();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка авторизации");
            alert.setHeaderText(ex.getMessage());
            alert.showAndWait();
        }
    }
}
