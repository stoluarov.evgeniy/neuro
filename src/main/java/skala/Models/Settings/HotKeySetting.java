package skala.Models.Settings;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.input.KeyEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HotKeySetting {
    /** Программное обозначение события */
    @JacksonXmlProperty(isAttribute = true)
    String name;
    /** Описание для пользователя */
    @JacksonXmlProperty(isAttribute = true)
    String label;
    /** Список комбинаций горячих клавиш */
    @JacksonXmlText
    String code;
    public SimpleStringProperty labelProperty() { return new SimpleStringProperty(label); }
    public SimpleStringProperty code1Property() {
        var c = code.split(";");
        return new SimpleStringProperty(c.length > 0? c[0] : "");
    }
    public SimpleStringProperty code2Property() {
        var c = code.split(";");
        return new SimpleStringProperty(c.length >= 2? c[1] : ""); }
    public static String KeyEventParser(KeyEvent keyEvent){
        return (keyEvent.isControlDown() ? "Ctrl + " : "")
                + (keyEvent.isAltDown() ? "Alt + " : "")
                + (keyEvent.isShiftDown() ? "Shift + " : "")
                + keyEvent.getCode().getName();
    }
    public void setCode1(String new_val){ code = new_val + ";" + code2Property().get(); }
    public void setCode2(String new_val){ code = (code1Property().get() + ";" + new_val).replaceFirst("^;", ""); }
    public void clear_code(String val){
        code = String.join(";",
                Arrays.stream(code.split(";")).filter(c -> !c.isBlank() && !c.equals(val)).toList());
    }
    public HotKeySetting(String name, String label) {
        this.name = name;
        this.label = label;
        this.code = "";
    }
}
