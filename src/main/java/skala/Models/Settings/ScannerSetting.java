package skala.Models.Settings;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ScannerSetting extends DefaultDeviceSettings {
    String scanner_name;
    int mode;
    int resolutions;
    int x, y, width, height;

    public ScannerSetting(){}

    public ScannerSetting(String scanner_name, int mode, int resolutions, int x, int y, int width, int height) {
        this.scanner_name = scanner_name;
        this.mode = mode;
        this.resolutions = resolutions;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
