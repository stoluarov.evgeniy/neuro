package skala.Models.Settings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Neuro;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@JacksonXmlRootElement(localName = "settings")
public class Settings {
    private static final Logger logger = LoggerFactory.getLogger(Neuro.class);
    @JacksonXmlProperty
    private ScannerSetting scanner;
    @JacksonXmlProperty
    private CameraSettings camera;
    @JacksonXmlElementWrapper(localName = "hot_keys")
    private List<HotKeySetting> hot_keys;
    @JacksonXmlElementWrapper(localName = "flags")
    private final HashMap<String, Boolean> flags;
    @JacksonXmlProperty
    private String browser;
    public Settings(){
        flags = new HashMap<>();
    }
    @JsonIgnore
    public void setSettings(DefaultDeviceSettings settings) {
        if(settings instanceof ScannerSetting scannerSetting)
            this.scanner = scannerSetting;
        else if (settings instanceof CameraSettings cameraSettings) {
            this.camera = cameraSettings;
        }
    }
    @JsonIgnore
    public Optional<DefaultDeviceSettings> getSettings(){
        if(scanner!=null) return Optional.of(scanner);
        else if(camera != null) return Optional.of(camera);
        return Optional.empty();
    }
    public void setScanner(ScannerSetting scanner) { this.scanner = scanner; }
    public void setCamera(CameraSettings camera) { this.camera = camera; }
    /** Загрузить из файла */
    public static Optional<Settings> load() {
        try {
            File file = new File("settings.xml");
            XmlMapper xmlMapper = new XmlMapper();
            return Optional.of(xmlMapper.readValue(file, Settings.class));
        }
        catch (IOException ignored){}
        catch (Exception ex)
        {
            logger.error("Settings - load", ex);
        }
        return Optional.empty();
    }
    /** Сохранить в файл */
    public void save() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        xmlMapper.writeValue(new File("settings.xml"), this);
    }
    public void setHot_keys(List<HotKeySetting> hot_keys) { this.hot_keys = hot_keys; }
    public List<HotKeySetting> generate_hotkeys_list(){
        List<HotKeySetting> default_list = generate_default_hotkeys_list();
        for(HotKeySetting key_settings: hot_keys){
            default_list.stream().filter(c -> c.name.equals(key_settings.name)).findFirst().ifPresent(c -> {
                c.label = key_settings.label;
                c.code = key_settings.code != null ? key_settings.code : "";
            });
        }
        return default_list;
    }
    /** Сгенерировать HashMap горячих клавиш для рабочего использования */
    public HashMap<String, String> generate_hotkeys_map(){
        var ret = new HashMap<String, String>();
        var hotkeys = generate_hotkeys_list();
        for(var keys : hotkeys)
            for(var code : keys.code.split(";"))
                if(!code.isEmpty())
                    ret.put(code, keys.name);
        return ret;
    }
    /** Сгенерировать начальный список горячих клавиш */
    public static List<HotKeySetting> generate_default_hotkeys_list(){
        var res = new ArrayList<HotKeySetting>();
        res.add(new HotKeySetting("new_rg", "Новый РГ"));
        res.add(new HotKeySetting("new_ig", "Новый ИГ"));
        res.add(new HotKeySetting("new_scan", "Сканировать"));
        res.add(new HotKeySetting("export_send", "Отправить"));
        res.add(new HotKeySetting("recognition_send", "Распознать"));
        return res;
    }
    public void setFlag(String name, boolean value){ flags.put(name, value); }
    public boolean getFlag(String name){
        return flags.getOrDefault(name, false);
    }
}
