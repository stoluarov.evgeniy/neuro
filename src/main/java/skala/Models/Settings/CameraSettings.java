package skala.Models.Settings;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CameraSettings extends DefaultDeviceSettings {
    String camera_name;
    int frequency;
    int width, height;
    public CameraSettings(){}

    public CameraSettings(String camera_name, int frequency, int width, int height) {
        this.camera_name = camera_name;
        this.frequency = frequency;
        this.width = width;
        this.height = height;
    }
}
