package skala.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.input.KeyCode;
import lombok.Getter;
import skala.Models.Dictionary.Dictionaries;
import skala.Models.Dictionary.DictionaryValue;
import skala.Models.Settings.DefaultDeviceSettings;
import skala.Models.Settings.Settings;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class WorkingSetting {
    @Getter
    private static final DictionaryValue[] person_types = new DictionaryValue[]{
            new DictionaryValue(0, "РГ"),
            new DictionaryValue(1, "ИГ")
    };
    @Getter
    private static final ObservableList<DictionaryValue> documents_rg = FXCollections.observableArrayList();
    @Getter
    private static final ObservableList<DictionaryValue> documents_ig = FXCollections.observableArrayList();
    @Getter
    private static final ObservableList<DictionaryValue> documents_all = FXCollections.observableArrayList();
    @Getter
    private static final flags Flags = new flags();
    /** Список клавиш, которые не могут быть использованы как горячие клавиши */
    public static final KeyCode[] hotkeys_ignore_list = new KeyCode[]{ KeyCode.ESCAPE, KeyCode.DELETE,
            KeyCode.SHIFT, KeyCode.ALT, KeyCode.CONTROL, KeyCode.CAPS, KeyCode.TAB, KeyCode.UNDEFINED,
            KeyCode.RIGHT, KeyCode.LEFT, KeyCode.UP, KeyCode.DOWN
    };
    @Getter
    private static HashMap<String, String> hotkeys_map;
    @Getter
    private static DefaultDeviceSettings device_settings;
    public static SimpleDateFormat date_formatter = new SimpleDateFormat("dd.MM.yyyy");

    public static void initSettings(DefaultDeviceSettings default_device_settings) {
        var dictionaries = Dictionaries.getDictionaries();
        documents_rg.addAll(dictionaries.getDictionary("Документы РГ"));
        documents_ig.addAll(dictionaries.getDictionary("Документы ИГ"));
        documents_all.addAll(documents_rg);
        documents_all.addAll(documents_ig);
        Settings.load().ifPresent(settings -> {
            hotkeys_map = settings.generate_hotkeys_map();
            device_settings = settings.getSettings().orElse(default_device_settings);
            Flags.bd_save = settings.getFlag("bd_save");
            Flags.auto_parse = settings.getFlag("auto_parse");
            Flags.auto_send = settings.getFlag("auto_send");
        });
    }
}
