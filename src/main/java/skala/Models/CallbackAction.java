package skala.Models;

@FunctionalInterface
public interface CallbackAction {
    void execute();
}
