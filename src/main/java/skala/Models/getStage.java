package skala.Models;

import javafx.stage.Window;

@FunctionalInterface
public interface getStage {
    Window get();
}
