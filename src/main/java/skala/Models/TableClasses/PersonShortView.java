package skala.Models.TableClasses;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.NoArgsConstructor;
import skala.Import.BDService;
import skala.Import.Entities.Person;
import skala.Models.LongTermOperationStatus;
import skala.Models.WorkingSetting;

import java.util.UUID;

@NoArgsConstructor
public class PersonShortView {
    UUID id;
    int type;
    SimpleIntegerProperty count;
    SimpleObjectProperty<LongTermOperationStatus> export_status;
    ObservableList<ScanView> scans;

    public PersonShortView(int type) {
        id = UUID.randomUUID();
        this.type = type;
        this.count = new SimpleIntegerProperty(0);
        scans = FXCollections.observableArrayList();
        export_status = new SimpleObjectProperty<>(LongTermOperationStatus.NO_OP);
    }
    public UUID getId() { return id; }
    public void setId(UUID id) { this.id = id; }
    public SimpleIntegerProperty typeProperty() { return new SimpleIntegerProperty(type); }
    public SimpleIntegerProperty countProperty() { return count; }
    public LongTermOperationStatus getExport_status() { return export_status.get(); }
    public SimpleObjectProperty<LongTermOperationStatus> export_statusProperty() { return export_status; }

    public ObservableList<ScanView> getScans() { return scans; }
    public int getType() { return type; }
    public void setExport_status(LongTermOperationStatus export_status) { this.export_status.set(export_status); }

    public void addScan(ScanView new_scan){
        if(WorkingSetting.getFlags().bd_save) {
            if(!BDService.addScan(id, new_scan)) return;
            new_scan.setImage(null);
        }
        scans.add(new_scan);
        count.set(scans.size());
    }
    public void removeScan(ScanView deleting_scan){
        if(deleting_scan == null) return;
        if(!WorkingSetting.getFlags().bd_save || BDService.deleteScanImage(deleting_scan.id))
        {
            scans.remove(deleting_scan);
            count.set(scans.size());
        }
    }
    public void removeAllScans(){
        scans.clear();
        count.set(0);
    }
    public Person toPerson(){
        var person = new Person();
        person.setId(id);
        person.setType(type);
        person.setSent(export_status.get().equals(LongTermOperationStatus.SUCCESS));
        return person;
    }
    public PersonShortView(Person person){
        id = person.getId();
        type = person.getType();
        scans = FXCollections.observableArrayList(person.getScans());
        count = new SimpleIntegerProperty(scans.size());
        export_status = new SimpleObjectProperty<>(person.isSent()? LongTermOperationStatus.SUCCESS:LongTermOperationStatus.NO_OP);
    }
}
