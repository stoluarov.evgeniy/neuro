package skala.Models.TableClasses;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;
import skala.Import.BDService;
import skala.Models.LongTermOperationStatus;
import skala.Models.WorkingSetting;

import java.util.UUID;

public class ScanView {
    @Getter
    UUID id;
    @Setter
    @Getter
    int type;
    SimpleObjectProperty<LongTermOperationStatus> recognition_status;
    @Setter
    Image image;
    public ScanView(int type, Image image) {
        id = UUID.randomUUID();
        this.type = type;
        this.image = image;
    }

    public ScanView(UUID id, int type, boolean parsed) {
        this.id = id;
        this.type = type;
        recognition_status = new SimpleObjectProperty<>(parsed? LongTermOperationStatus.SUCCESS:LongTermOperationStatus.NO_OP);
    }
    public Image getImage() {
        return image != null ? image : WorkingSetting.getFlags().bd_save? BDService.getScanImage(id):null;
    }
    public LongTermOperationStatus getRecognition_status() { return recognition_status.get(); }
    public SimpleObjectProperty<LongTermOperationStatus> recognition_statusProperty() { return recognition_status; }
    public void setRecognition_status(LongTermOperationStatus recognition_status) { this.recognition_status.set(recognition_status); }
}
