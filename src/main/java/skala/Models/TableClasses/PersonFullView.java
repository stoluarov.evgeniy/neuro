package skala.Models.TableClasses;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;
import java.time.LocalDate;

import static skala.Models.WorkingSetting.date_formatter;

public class PersonFullView {
    int type;
    SimpleStringProperty surname;
    SimpleStringProperty name;
    SimpleStringProperty middlename;
    SimpleObjectProperty<LocalDate> date_from;
    SimpleObjectProperty<LocalDate> date_to;
    SimpleIntegerProperty scans_count;
    public PersonFullView(PeopleType type, String surname, String name, String middlename) {
        this.type = type.equals(PeopleType.RUSSIAN)? 0: 1;
        this.surname = new SimpleStringProperty(surname);
        this.name = new SimpleStringProperty(name);
        this.middlename = new SimpleStringProperty(middlename);
        this.date_from = new SimpleObjectProperty<>(LocalDate.now());
        this.date_to = new SimpleObjectProperty<>(LocalDate.now());
        this.scans_count = new SimpleIntegerProperty(0);
    }

    public int getType() { return type; }
    public String getSurname() { return surname.get(); }
    public SimpleStringProperty surnameProperty() { return surname; }
    public String getName() { return name.get(); }
    public SimpleStringProperty nameProperty() { return name; }
    public String getMiddlename() { return middlename.get(); }
    public SimpleStringProperty middlenameProperty() { return middlename; }
    public LocalDate getDate_from() { return date_from.get(); }
    public SimpleObjectProperty<LocalDate> date_fromProperty() { return date_from; }
    public LocalDate getDate_to() { return date_to.get(); }
    public SimpleObjectProperty<LocalDate> date_toProperty() { return date_to; }
    public int getScans_count() { return scans_count.get(); }
    public SimpleIntegerProperty scans_countProperty() { return scans_count; }

    public void setType(int type) { this.type = type; }
    public void setType(PeopleType type) { this.type = type.equals(PeopleType.RUSSIAN)? 0: 1; }
    public void setSurname(String surname) { this.surname = new SimpleStringProperty(surname); }
    public void setName(String name) { this.name = new SimpleStringProperty(name); }
    public void setMiddlename(String middlename) { this.middlename = new SimpleStringProperty(middlename); }
    public void setDate_from(LocalDate date_from) { this.date_from = new SimpleObjectProperty<>(date_from); }
    public void setDate_to(LocalDate date_to) { this.date_to = new SimpleObjectProperty<>(date_to); }
    public void setScans_count(int scans_count) { this.scans_count = new SimpleIntegerProperty(scans_count); }
}
