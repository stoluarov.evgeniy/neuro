package skala.Models.TableClasses;

import javafx.event.Event;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDate;
import java.time.format.FormatStyle;

public class DateCell<S> extends ComboBoxTableCell<S, LocalDate> {

    private DatePicker field;
    private boolean escapePressed = false;
    private TablePosition<S, ?> tablePos = null;
    public DateCell(final StringConverter<LocalDate> converter) { super(converter); }
    public static <S> Callback<TableColumn<S, LocalDate>, TableCell<S, LocalDate>> forTableColumn() {
        return forTableColumn(new LocalDateStringConverter(FormatStyle.SHORT));
    }
    public static <S> Callback<TableColumn<S, LocalDate>, TableCell<S, LocalDate>> forTableColumn(final StringConverter<LocalDate> converter) {
        return list -> new DateCell<>(converter);
    }
    @Override
    public void startEdit() {
        if (!isEditable() || !getTableView().isEditable() || !getTableColumn().isEditable()) return;
        super.startEdit();

        if (isEditing()) {
            if (field == null) field = getDateField();
            escapePressed = false;
            startEdit(field);
            final TableView<S> table = getTableView();
            tablePos = table.getEditingCell();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void commitEdit(LocalDate newValue) {
        if (!isEditing())
            return;
        final TableView<S> table = getTableView();
        if (table != null) {
            // Inform the TableView of the edit being ready to be committed.
            TableColumn.CellEditEvent editEvent = new TableColumn.CellEditEvent(table, tablePos, TableColumn.editCommitEvent(), newValue);
            Event.fireEvent(getTableColumn(), editEvent);
        }
        // we need to setEditing(false):
        super.cancelEdit(); // this fires an invalid EditCancelEvent.
        // update the item within this cell, so that it represents the new value
        updateItem(newValue, false);
        if (table != null) {
            // reset the editing cell on the TableView
            table.edit(-1, null);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void cancelEdit() {
        if (escapePressed) {
            // this is a cancel event after escape key
            field.setValue(getItem());
            super.cancelEdit();
            setText(getItemText()); // restore the original text in the view
        } else {
            // this is not a cancel event after escape key
            // we interpret it as commit.
            String newText = field.getConverter().toString(field.getValue());
            // commit the new text to the model
            this.commitEdit(getConverter().fromString(newText));
        }
        setGraphic(null); // stop editing with TextField
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void updateItem(LocalDate item, boolean empty) {
        super.updateItem(item, empty);
        updateItem();
    }
    private DatePicker getDateField() {

        final DatePicker field = new DatePicker(getItemValue());
        // Use onAction here rather than onKeyReleased (with check for Enter),
        field.setOnAction(event -> {
            if (getConverter() == null) throw new IllegalStateException("StringConverter is null.");
            this.commitEdit(field.getValue());
            event.consume();
        });

        field.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) commitEdit(field.getValue());
        });

        field.setOnKeyPressed(t -> escapePressed = t.getCode() == KeyCode.ESCAPE);
        /*textField.setOnKeyReleased(t -> {
            if (t.getCode() == KeyCode.ESCAPE) throw new IllegalArgumentException("did not expect esc key releases here.");
        });*/

        field.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            } else if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.TAB)
                getTableView().getSelectionModel().selectNext();
            else if (event.getCode() == KeyCode.LEFT)
                getTableView().getSelectionModel().selectPrevious();
            else if (event.getCode() == KeyCode.UP)
                getTableView().getSelectionModel().selectAboveCell();
            else if (event.getCode() == KeyCode.DOWN)
                getTableView().getSelectionModel().selectBelowCell();
            event.consume();
        });
        return field;
    }
    private LocalDate getItemValue() {
        return getItem();
    }
    private String getItemText() {
        return getConverter() == null ?
                getItem() == null ? "" : getItem().toString() :
                getConverter().toString(getItem());
    }
    private void updateItem() {
        if (isEmpty()) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (field != null) field.setValue(getItemValue());
                setText(null);
                setGraphic(field);
            } else {
                setText(getItemText());
                setGraphic(null);
            }
        }
    }
    private void startEdit(final DatePicker field) {
        if (field != null) field.setValue(getItemValue());
        setText(null);
        setGraphic(field);
        // requesting focus so that key input can immediately go into the
        // TextField
        field.requestFocus();
    }
}