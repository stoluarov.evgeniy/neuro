package skala.Models.TableClasses;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Controllers.ScanningController;
import skala.Models.Dictionary.DictionaryValue;
import skala.Neuro;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.NoSuchElementException;

public class ComboBoxCell{
    private static final Logger logger = LoggerFactory.getLogger(ComboBoxCell.class);
    public static<view> Callback<TableColumn.CellDataFeatures<view, DictionaryValue>, ObservableValue<DictionaryValue>> getCellValueFactory(List<DictionaryValue> dictionary, String field_name) throws RuntimeException{
        return person -> {
            try {
                person.getValue().getClass().getDeclaredField(field_name).setAccessible(true);
                var value = (int) person.getValue().getClass().getDeclaredField(field_name).get(person.getValue());
                return new SimpleObjectProperty<>(dictionary.stream().filter(c -> c.getValue() == value).findFirst().orElseThrow());
            }
            catch (NoSuchElementException ex){
                logger.error("Value not found in dictionary", ex);
                return new SimpleObjectProperty<>();
            }
            catch (NoSuchFieldException | IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        };
    }
    public static<view> EventHandler<TableColumn.CellEditEvent<view, DictionaryValue>> getOnEditCommit(String set_field_method) throws RuntimeException{
        return event -> {
            try {
            TablePosition<view, DictionaryValue> pos = event.getTablePosition();
            DictionaryValue new_value = event.getNewValue();
                view entity = event.getTableView().getItems().get(pos.getRow());
                entity.getClass().getMethod(set_field_method, int.class).invoke(entity, new_value.getValue());
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
                throw new RuntimeException(ex);
            }
        };
    }
}
