package skala.Models.DeviceInterfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/** Возможные режимы работы сканера */
public class ScannerModeView {
    private final HashMap<Integer, String> scanner_modes = new HashMap<>();

    public ScannerModeView() {
        scanner_modes.put(8, "RGB - 8");
        scanner_modes.put(-8, "GRAY - 8");
        scanner_modes.put(16, "RGB - 16");
        scanner_modes.put(-16, "GRAY - 16");
        scanner_modes.put(-1, "BLACK AND WHITE");
        scanner_modes.put(0, "DUPLEX NOT AVAIL");
        scanner_modes.put(1, "DUPLEX AVAIL");
        scanner_modes.put(2, "DUPLEX_AVAIL_FRONT_ONLY");
        scanner_modes.put(3, "DUPLEX_AVAIL_BACK_ONLY");
    }
    public List<String> getNames(List<Integer> values){
        var res = new ArrayList<String>();
        res.add(null);
        values.forEach(c -> res.add(scanner_modes.get(c)));
        return res.stream().filter(Objects::nonNull).toList();
    }
    public Integer getValue(String Name) throws ArrayStoreException{
        for(var pair : scanner_modes.entrySet())
            if(pair.getValue().equals(Name))
                return pair.getKey();
        throw new ArrayStoreException("Режим работы сканера не найден");
    }
}
