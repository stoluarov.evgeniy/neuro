package skala.Models.DeviceInterfaces;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import skala.Models.getStage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FolderViewer extends ImageViewer<FileChooser>{
    getStage stage;
    public FolderViewer(ImageView output) {
        super(output);
    }
    public FolderViewer(ImageView output, FolderSettings settings) {
        super(output);
        device = new FileChooser();
        device.setTitle("Open a file");
        device.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All image files","*.jpg","*.png"));
        this.stage = settings.stage;
    }
    @Override
    public void reloadDevice() {}
    @Override
    public void clearDevice() {}
    @Override
    public List<Image> getImage() {
        List<File> selected_files = device.showOpenMultipleDialog(stage.get());
        ArrayList<Image> ret = new ArrayList<>();
        if(selected_files == null) return ret;
        for (var file :  selected_files){
            try(FileInputStream fis = new FileInputStream(file)) {
                ret.add(new Image(fis));
            }
            catch (IOException ignored){ }
        }
        return ret;
    }
    @Override
    public void stop() { }
}
