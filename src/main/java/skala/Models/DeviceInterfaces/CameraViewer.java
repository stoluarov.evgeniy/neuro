package skala.Models.DeviceInterfaces;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamLockException;
import com.github.sarxos.webcam.WebcamResolution;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import skala.Models.Settings.CameraSettings;

import java.awt.*;
import java.util.*;
import java.util.List;

import static skala.Neuro.show_error;

public class CameraViewer extends ImageViewer<Webcam>{
    CameraSettings settings;
    private final int period;
    private Timer timer;

    public CameraViewer(ImageView output, CameraSettings settings) {
        super(output);
        this.settings = settings;
        period = 1000 / settings.getFrequency();
        reloadDevice();
    }
    /** получить список доступных камер */
    public static List<String> getDevices(){ return Webcam.getWebcams().stream().map(Webcam::getName).toList(); }

    /**
     * Получить камеру по названию
     * @param name имя камеры
     * @return объект для работы с камерой
     */
    public static Optional<Webcam> getDevice(String name){
        return Webcam.getWebcams().stream().filter(c -> c.getName().equals(name)).findFirst();
    }
    /** {@inheritDoc} */
    @Override
    public void reloadDevice() {
        if(device!=null) clearDevice();
        try {
            device = Webcam.getWebcamByName(settings.getCamera_name());
            Arrays.stream(device.getViewSizes()).filter(c -> c.width == settings.getWidth() && c.getHeight() == settings.getHeight()).findFirst().ifPresent(c -> device.setViewSize(c));
        }
        catch (Exception ex)
        {
            show_error("Не удалось проинициализировать камеру, проверьте подключение");
        }
    }
    /** {@inheritDoc} */
    @Override
    public void clearDevice() {
        stop();
        if(device != null)
            device.close();
        timer = null;
    }
    /** {@inheritDoc} */
    @Override
    public List<Image> getImage() {
        if(!device.isOpen()) device.open();
        return List.of(SwingFXUtils.toFXImage(device.getImage(), null));
    }
    /** {@inheritDoc} */
    public void start() {
        if(device == null) return;
        if(!device.isOpen()) device.open();
        timer = new Timer("NeuroCamera");
        timer.schedule(new TimerTask() {
            @Override
            public void run() { uploadImage(); }
        }, 0, period);
    }
    /** {@inheritDoc} */
    public void stop() {
        if (timer != null) timer.cancel();
    }
    /** {@inheritDoc} */
    public CameraSettings getSettings(){ return this.settings; }
}