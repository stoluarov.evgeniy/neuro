package skala.Models.DeviceInterfaces;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import skala.Models.Settings.CameraSettings;
import skala.Models.Settings.DefaultDeviceSettings;
import skala.Models.Settings.ScannerSetting;

import java.util.ArrayList;
import java.util.List;

/** Интерфейс для работы с устройствами ввода */
public abstract class ImageViewer<T> {
    final ImageView photoArea;
    T device;
    public ImageViewer(ImageView output) { this.photoArea = output; }
    public static List<String> getDevices(){
        var res = new ArrayList<String>();
        res.addAll(CameraViewer.getDevices());
        res.addAll(ScannerViewer.getDevices());
        res.add("Из папки");
        return res;
    }
    /**
     * Генерация подключения по выбранным настройкам
     * @param output место для вывода изображения
     * @param settings настройки для инициализации устройства
     * @return Интерфейс для работы с устройством
     */
    public static ImageViewer getDevice(ImageView output, DefaultDeviceSettings settings){
        if(settings instanceof CameraSettings cs)
            return getDevice(output, cs);
        else if (settings instanceof ScannerSetting ss)
            return getDevice(output, ss);
        else if (settings instanceof FolderSettings fs)
            return getDevice(output, fs);
        return null;
    }
    public static CameraViewer getDevice(ImageView output, CameraSettings settings){
        return new CameraViewer(output, settings);
    }
    public static ScannerViewer getDevice(ImageView output, ScannerSetting settings){
        return new ScannerViewer(output, settings);
    }
    public static FolderViewer getDevice(ImageView output, FolderSettings settings){
        return new FolderViewer(output, settings);
    }

    /** Переподключить устройство */
    public abstract void reloadDevice();
    /** Освободить ресурсы устройства */
    public abstract void clearDevice();
    /** Отобразить изображение с устройства в форме */
    public void uploadImage(){ getImage().stream().findFirst().ifPresent(photoArea::setImage); }
    /** Получить изображение с устройства */
    public abstract List<Image> getImage();
    /** Запустить устройство */
    public void start() { }
    /** Остановить устройство */
    public abstract void stop();
    /** Получить настройки устройства */
    public DefaultDeviceSettings getSettings(){ return new DefaultDeviceSettings(); }
}