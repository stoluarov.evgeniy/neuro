package skala.Models.DeviceInterfaces;

import eu.gnome.morena.Device;
import eu.gnome.morena.Manager;
import eu.gnome.morena.Scanner;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import skala.Models.Settings.ScannerSetting;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static skala.Neuro.show_error;

public class ScannerViewer extends ImageViewer<Device> {
    ScannerSetting settings;
    public ScannerViewer(ImageView output, ScannerSetting settings) {
        super(output);
        this.settings = settings;
        reloadDevice();
    }
    /** получить список доступных камер */
    public static List<String> getDevices(){
        Manager manager=Manager.getInstance();
        return manager.listDevices().stream().map(Device::toString).toList();
    }
    /**
     * Получить камеру по названию
     * @param name имя камеры
     * @return объект для работы с камерой
     */
    public static Optional<Scanner> getDevice(String name){
        Manager manager=Manager.getInstance();
        var list = manager.listDevices().stream().filter(c -> c.toString().equals(name)).toList();
        for(var d : list)
            if(d instanceof Scanner s)
                return Optional.of(s);
        return Optional.empty();
    }
    /** {@inheritDoc} */
    @Override
    public void reloadDevice() {
        Manager manager=Manager.getInstance();
        manager.listDevices().stream().filter(c -> c.toString().equals(settings.getScanner_name())).findFirst().ifPresent(c -> device = c);
        if(device instanceof Scanner scanner){
            scanner.setMode(settings.getMode());
            scanner.setResolution(settings.getResolutions());
            scanner.setFrame(settings.getX(), settings.getY(), settings.getX()+settings.getWidth(), settings.getY()+settings.getHeight());
        }
    }
    /** {@inheritDoc} */
    @Override
    public void clearDevice() {
        device = null;
    }
    /** {@inheritDoc} */
    @Override
    public List<Image> getImage() {
        try {
            BufferedImage bimage = SynchronousHelper.scanImage(device);
            return List.of(SwingFXUtils.toFXImage(bimage, null));
        }
        catch (Exception ex){
            show_error("Ошибка работы сканера, проверьте подключение");
        }
        return new ArrayList<>();
    }
    /** {@inheritDoc} */
    @Override
    public void stop() {}
    /** {@inheritDoc} */
    public ScannerSetting getSettings(){ return this.settings; }
}