package skala.Models.DeviceInterfaces;

import javafx.stage.Window;
import skala.Models.Settings.DefaultDeviceSettings;
import skala.Models.getStage;

public class FolderSettings extends DefaultDeviceSettings {
    getStage stage;
    String start_folder = "";
    public FolderSettings(getStage stage) { this.stage = stage; }
    public getStage getStage() { return stage; }
    public void setStage(getStage stage) { this.stage = stage; }

    public String getStart_folder() { return start_folder; }
    public void setStart_folder(String start_folder) { this.start_folder = start_folder; }
}
