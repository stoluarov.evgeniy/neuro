package skala.Models.DeviceInterfaces;
/** Разрешение (W x H) */
public class DimensionView {
    int width;
    int height;
    public DimensionView(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public int getWidth() { return width; }
    public void setWidth(int width) { this.width = width; }
    public int getHeight() { return height; }
    public void setHeight(int height) { this.height = height; }
    @Override
    public String toString() { return width + " x " + height; }
}
