package skala.Models.Dictionary;

import javafx.util.StringConverter;

public class DictionaryConverter extends StringConverter<DictionaryValue> {
    @Override
    public String toString(DictionaryValue value) {
        return value != null ? value.getText() : null;
    }

    @Override
    public DictionaryValue fromString(String s) {
        return new DictionaryValue(-1, s);
    }
}
