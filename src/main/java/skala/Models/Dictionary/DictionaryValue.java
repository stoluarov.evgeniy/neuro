package skala.Models.Dictionary;

import javafx.beans.property.SimpleStringProperty;

public class DictionaryValue {
    /** Сохраняемое справочника */
    Integer value;
    /** Приоритет вывода в список */
    Integer num;
    /** Выводимый текст */
    String text;

    public DictionaryValue() { }
    public DictionaryValue(int value, String text) {
        this.value = value;
        this.text = text;
        this.num = value;
    }
    public int getValue() { return value; }
    public String getText() { return text; }
    public int getNum() { return num; }
    public void setValue(Integer value) { this.value = value; }
    public void setText(String text) { this.text = text; }
    public void setNum(Integer num) { this.num = num; }
}
