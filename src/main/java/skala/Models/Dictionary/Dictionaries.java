package skala.Models.Dictionary;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Контейнер для работы со словарями */
public class Dictionaries {
    List<Dictionary> dictionaries = new ArrayList<>();
    /**
     * Получить словарь по его названию
     * @param name название словаря
     * @return словарь как объект
     */
    public Dictionary getDictionaryObject(String name){ return dictionaries.stream().filter(c -> c.name.equals(name)).findFirst().orElseGet(() -> new Dictionary(name)); }

    /**
     * Получить значения словаря по его названию
     * @param name название словаря
     * @return список значений
     */
    public List<DictionaryValue> getDictionary(String name){ return getDictionaryObject(name).getValues(); }
    public List<Dictionary> getDictionaryList(){ return dictionaries; }

    public Dictionaries() {
    }
    /** Сгенерировать начальный словарь */
    public static Dictionaries getDefaultDictionaries(){
        var ret = new Dictionaries();
        ret.dictionaries.add(new Dictionary("Документы РГ", List.of(
                new DictionaryValue(0, "Паспорт РФ"),
                new DictionaryValue(1, "Свидетельство о рождении"),
                new DictionaryValue(2, "Паспорт гражданина СССР"))));
        ret.dictionaries.add(new Dictionary("Документы ИГ", List.of(
                new DictionaryValue(0, "Иностранный паспорт"),
                new DictionaryValue(1, "Виза"),
                new DictionaryValue(2, "Миграционная карта"))));
        return ret;
    }
    /** Загрузить словари */
    public static Dictionaries getDictionaries(){
        var ret = new Dictionaries();
        try {
            ret.dictionaries = new ObjectMapper().readValue(new File("dictionaries.json"), new TypeReference<>() {});
        }
        catch (IOException ex){
            ret = getDefaultDictionaries();
        }
        return ret;
    }
    /** Сохранить словари в файл */
    public static void save_dictionaries(List<Dictionary> dictionaries) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(new File("dictionaries.json"), dictionaries);
    }
}

