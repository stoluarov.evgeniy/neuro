package skala.Models.Dictionary;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

/** Словарь значений */
public class Dictionary {
    /** Название словаря */
    String name;
    /** Список значений */
    ObservableList<DictionaryValue> values;

    public Dictionary() { }
    public Dictionary(String name) {
        this.name = name;
        values = FXCollections.observableArrayList();
    }

    public Dictionary(String name, List<DictionaryValue> values) {
        this.name = name;
        this.values = FXCollections.observableArrayList(values);
    }

    public String getName() { return name; }
    public ObservableList<DictionaryValue> getValues() { return values; }
    public void setName(String name) { this.name = name; }
    public void setValues(List<DictionaryValue> values) { this.values = FXCollections.observableArrayList(values); }
    /** Добавить значение в словарь */
    public void addValue(DictionaryValue value) { this.values.add(value); }

    @Override
    public String toString() { return name; }
}
