package skala.Models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import skala.Models.Settings.Settings;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

@JacksonXmlRootElement(localName = "user_info")
public class UserDetails {
    @JacksonXmlProperty
    private String login;
    @JacksonXmlProperty
    private String password;

    public UserDetails() { }
    public UserDetails(String login, String password) throws IllegalArgumentException {
        if(login.equals("login") && password.length() > 4){
            this.login = login;
            this.password = password;
            return;
        }
        throw new IllegalArgumentException("Неверные логин или пароль");
    }
    public boolean validate(String login, String password){
        if(login.equals("login") && password.length() > 4){
            this.login = login;
            this.password = password;
            return true;
        }
        return false;
    }
    public String getLogin() { return login; }
    public String getPassword() { return password; }
    /** Загрузить из файла */
    public static Optional<UserDetails> load(){
        try {
            File file = new File("user_info.xml");
            XmlMapper xmlMapper = new XmlMapper();
            return Optional.of(xmlMapper.readValue(file, UserDetails.class));
        }
        catch (IOException ignored){}
        return Optional.empty();
    }
    /** Сохранить в файл */
    public void save() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        xmlMapper.writeValue(new File("user_info.xml"), this);
    }
}
