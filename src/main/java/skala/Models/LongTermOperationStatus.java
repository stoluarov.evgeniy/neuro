package skala.Models;

public enum LongTermOperationStatus {
    NO_OP, PROCESSING, ERROR, SUCCESS
}
