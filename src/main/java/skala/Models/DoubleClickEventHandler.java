package skala.Models;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.Timer;
import java.util.TimerTask;

/** Обработчик двойных нажатий */
public class DoubleClickEventHandler implements EventHandler<MouseEvent> {
    /** Период ожидания второго нажатия */
    private final int delay = 300;
    /** Действие при одинарном нажатии */
    private final Command onClick;
    /** Действие при двойном нажатии */
    private final Command onDoubleClick;
    /** Количество нажатий */
    private int count = 0;

    @Override
    public void handle(MouseEvent event) {
        count++;
        if(count % 2 == 1) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                final int start = count;
                @Override
                public void run() {
                    if(start == count) {
                        onClick.execute(event);
                        count = 0;
                    }
                }

            }, delay);
        }else {
            onDoubleClick.execute(event);
            count = 0;
        }
    }

    /**
     * @param onClick Действие при одинарном нажатии
     * @param onDoubleClick Действие при двойном нажатии
     */
    public DoubleClickEventHandler(Command onClick, Command onDoubleClick) {
        this.onClick = onClick;
        this.onDoubleClick = onDoubleClick;
    }
    public interface Command{ void execute(MouseEvent event); }
}
