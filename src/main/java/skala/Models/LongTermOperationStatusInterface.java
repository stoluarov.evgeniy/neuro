package skala.Models;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import skala.Models.TableClasses.PersonShortView;

public class LongTermOperationStatusInterface {
    public static Image getImageByStatus(LongTermOperationStatus status) {
        return switch (status){
            case NO_OP -> new Image("skala/images/NO_OP.gif");
            case PROCESSING -> new Image("skala/images/PROCESSING.gif");
            case ERROR -> new Image("skala/images/ERROR.gif");
            case SUCCESS -> new Image("skala/images/SUCCESS.png");
        };
    }
    public static class StatusCell extends TableCell<PersonShortView, LongTermOperationStatus>{
        private final ImageView imageView = new ImageView();
        @Override
        protected void updateItem(LongTermOperationStatus status, boolean empty) {
            this.setAlignment(Pos.CENTER);
            super.updateItem(status, empty);
            if (empty || status == null) {
                setGraphic(null);
            } else {
                imageView.setImage(getImageByStatus(status));
                imageView.setFitWidth(14); // Set your preferred width
                imageView.setFitHeight(14); // Set your preferred height
                setGraphic(imageView);
            }
        }
    }
}
