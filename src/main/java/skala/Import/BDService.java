package skala.Import;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import skala.Import.Entities.Person;
import skala.Models.TableClasses.ScanView;
import skala.Neuro;

import javax.imageio.ImageIO;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static skala.Neuro.show_error;

public class BDService {
    private static final Logger logger = LoggerFactory.getLogger(Neuro.class);
    private static final String insert_person_command = "INSERT INTO PEOPLE (ID, NAME, SURNAME, MIDDLENAME, TYPE) VALUES (?, ?, ?, ?, ?)";
    private static final String insert_scan_command = "INSERT INTO SCANS (UID, TYPE, SCAN, PERSON_ID) VALUES (?, ?, ?, ?)";
    private static final String get_scan_command = "SELECT SCAN FROM SCANS WHERE UID = ?";
    private static final String get_people_command = "SELECT id, name, surname, middlename, type, is_sent FROM PEOPLE";
    private static final String get_scan_info_command = "SELECT UID, TYPE, IS_PARSED FROM SCANS WHERE PERSON_ID = ?";
    private static final String delete_scan_command = "DELETE FROM SCANS WHERE UID = ?";
    private static final String delete_person_command = "DELETE FROM PEOPLE WHERE ID = ?";
    private static final String update_person_status_command = "UPDATE PEOPLE SET IS_SENT = ? WHERE ID = ?";
    private static final String update_scan_status_command = "UPDATE SCANS SET IS_PARSED = ? WHERE UID = ?";
    private static Connection getLocalBDConnection() throws SQLException { return DriverManager.getConnection("jdbc:h2:./neuro_local"); }
    public static boolean addPerson(Person person){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(insert_person_command);
            stat.setString(1, person.getId().toString());
            stat.setString(2, person.getName());
            stat.setString(3, person.getSurname());
            stat.setString(4, person.getMiddlename());
            stat.setInt(5, person.getType());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - addPerson", ex);
            return false;
        }
    }
    public static boolean addScan(UUID person_id, ScanView scan){
        try(Connection conn = getLocalBDConnection()){
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(SwingFXUtils.fromFXImage(scan.getImage(), null), "png", os);
            PreparedStatement stat = conn.prepareStatement(insert_scan_command);
            stat.setString(1, scan.getId().toString());
            stat.setInt(2, scan.getType());
            stat.setBytes(3, os.toByteArray());
            stat.setString(4, person_id.toString());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException | IOException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - addScan", ex);
            return false;
        }
    }
    public static Image getScanImage(UUID scan_id){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(get_scan_command);
            stat.setString(1, scan_id.toString());
            var res = stat.executeQuery();
            if(res.next())
            {
                var image_array = res.getBytes(1);
                return SwingFXUtils.toFXImage(ImageIO.read(new ByteArrayInputStream(image_array)), null);
            }
        }
        catch (SQLException | IOException ex){
            logger.error("BDService - getScanImage", ex);
        }
        return null;
    }
    public static boolean deleteScanImage(UUID scan_id){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(delete_scan_command);
            stat.setString(1, scan_id.toString());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - deleteScanImage", ex);
            return false;
        }
    }
    public static boolean deletePerson(UUID person_id){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(delete_person_command);
            stat.setString(1, person_id.toString());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - deletePerson", ex);
            return false;
        }
    }
    public static boolean updatePersonStatus(boolean sent_status, UUID person_id){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(update_person_status_command);
            stat.setBoolean(1, sent_status);
            stat.setString(2, person_id.toString());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - updateScanStatus", ex);
            return false;
        }
    }
    public static boolean updateScanStatus(boolean parsed_status, UUID scan_id){
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(update_scan_status_command);
            stat.setBoolean(1, parsed_status);
            stat.setString(2, scan_id.toString());
            stat.executeUpdate();
            return true;
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - updateScanStatus", ex);
            return false;
        }
    }

    public static List<Person> getPeople(){
        var ret = new ArrayList<Person>();
        try(Connection conn = getLocalBDConnection()){
            PreparedStatement stat = conn.prepareStatement(get_people_command);
            var res = stat.executeQuery();
            while (res.next()){
                var new_person = new Person(UUID.fromString(res.getString("id")), res.getString("surname"), res.getString("name"),
                        res.getString("middlename"), res.getInt("type"), res.getBoolean("is_sent"));
                try{
                    PreparedStatement stat_2 = conn.prepareStatement(get_scan_info_command);
                    stat_2.setString(1, new_person.getId().toString());
                    var scans= stat_2.executeQuery();
                    while (scans.next())
                        new_person.addScan(new ScanView(UUID.fromString(scans.getString("UID")), scans.getInt("TYPE"), scans.getBoolean("IS_PARSED")));
                }
                catch (SQLException ex){
                    logger.error("BDService - getPeople", ex);
                }
                ret.add(new_person);
            }
        }
        catch (SQLException ex){
            show_error("Не удалось подключиться к базе данных", "Обратитесь к администратору");
            logger.error("BDService - getPeople", ex);
        }
        return ret;
    }
}
