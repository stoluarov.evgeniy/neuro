package skala.Import.Entities;

import lombok.Getter;
import lombok.Setter;
import skala.Models.TableClasses.PersonShortView;
import skala.Models.TableClasses.ScanView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Setter
@Getter
public class Person {
    UUID id;
    String name;
    String surname;
    String middlename;
    int type;
    boolean sent;
    List<ScanView> scans;
    public Person() { }
    public Person(UUID id, String surname, String name, String middlename, int type, boolean sent) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middlename = middlename;
        this.type = type;
        this.sent = sent;
        scans = new ArrayList<>();
    }
    public void addScan(ScanView scan){ scans.add(scan); }
}
