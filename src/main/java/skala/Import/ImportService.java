package skala.Import;

import skala.Models.LongTermOperationStatus;
import skala.Models.TableClasses.PersonShortView;
import skala.Models.WorkingSetting;

import java.util.Timer;
import java.util.TimerTask;

public class ImportService {
    private static void sendPerson(PersonShortView person){
        try {
            person.setExport_status(LongTermOperationStatus.PROCESSING);
            Thread.sleep(5 * 1000);
            person.setExport_status(Math.random() % 3 < 0.7 ? LongTermOperationStatus.SUCCESS : LongTermOperationStatus.ERROR);
            if(WorkingSetting.getFlags().bd_save && person.getExport_status().equals(LongTermOperationStatus.SUCCESS)) BDService.updatePersonStatus(true, person.getId());
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
    private static void recognizePerson(PersonShortView person){
        try {
            for(var scan : person.getScans()) {
                if(scan.getRecognition_status().equals(LongTermOperationStatus.SUCCESS)) continue;
                scan.setRecognition_status(LongTermOperationStatus.PROCESSING);
                Thread.sleep(5 * 1000);
                scan.setRecognition_status(Math.random() % 3 >= 0.7 ? LongTermOperationStatus.SUCCESS : LongTermOperationStatus.ERROR);
                if(WorkingSetting.getFlags().bd_save && scan.getRecognition_status().equals(LongTermOperationStatus.SUCCESS)) BDService.updateScanStatus(true, scan.getId());
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
    public static void sendPersonAsync(PersonShortView person){
        Timer timer = new Timer("Sending thread");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                sendPerson(person);
            }

        }, 0);
    }
    public static void recognizePersonAsync(PersonShortView person){
        Timer timer = new Timer("Sending thread");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                recognizePerson(person);
            }

        }, 0);
    }
}
