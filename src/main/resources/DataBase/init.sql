create table PEOPLE
(
    ID         UUID    default RANDOM_UUID() not null,
    NAME       CHARACTER VARYING(1000000000),
    SURNAME    CHARACTER VARYING(1000000000),
    MIDDLENAME CHARACTER VARYING(1000000000),
    TYPE       INTEGER,
    IS_SENT    BOOLEAN default FALSE         not null,
    constraint PEOPLE_PK
        primary key (ID)
);

create table SCANS
(
    UID       UUID                not null,
    TYPE      INTEGER             not null,
    SCAN      BINARY LARGE OBJECT not null,
    PERSON_ID UUID                not null,
    IS_PARSED  BOOLEAN default FALSE         not null,
    constraint SCANS_PK
        primary key (UID),
    constraint SCANS_PEOPLE_ID_FK
        foreign key (PERSON_ID) references PEOPLE
            on delete cascade
);